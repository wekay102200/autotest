#coding:utf-8
import requests
import yaml
import os
import sys
import time 
from config import *


GlobalData = {}
Cookie = None

TotalCases = 0
SuccCases = 0
FailCases = 0

EnableColor = True

try:
    import Color
except:
    EnableColor = False

def VerifyListMembers(name,member_data,real_data_list):
    '''
        验证data输出成员里面类型为list的成员名称和类型
    '''

    if 0 == len(member_data):
        return

    if 0 == len(real_data_list):
        raise ValueError("output member[%s] return list count is empty,but expect is not empty" % name)

    if CHECK_LIST_RULE == CHECK_LIST_FIRST:
       confirm_data = real_data_list[0]
    elif CHECK_LIST_RULE == CHECK_LIST_RANDOM:
        import random
        index = random.randint(0,len(real_data_list) - 1)
        confirm_data = real_data_list[index]

    for item in member_data.items():
        add_to_global = False
        add_global_name = None
        if item[1].has_key('export'):
            add_to_global = True
            add_global_name = item[1]['export']
        if CHECK_LIST_RULE == CHECK_LIST_ALL:
            for confirm_data in real_data_list:
                ConfirmData(item,confirm_data,add_to_global,add_global_name,name)
        else:
            ConfirmData(item,confirm_data,add_to_global,add_global_name,name)

def VerifyMapMembers(name,member_data,mp_real_data):
    '''
        验证data输出成员里面类型为dict的成员名称和类型
    '''

    if 0 == len(member_data):
        return

    if 0 == len(mp_real_data):
        raise ValueError("output member[%s] return map count is empty,but expect is not empty"% name)
    
    for item in member_data.items():
        add_to_global = False
        add_global_name = None
        if item[1].has_key('export'):
            add_to_global = True
            add_global_name = item[1]['export']
        ConfirmData(item,mp_real_data,add_to_global,add_global_name,name)

def LoadBaseConfig():
    '''
        加载基本配置,一些重复使用或公用的变量配置值,如用户名,密码，手机号等.
    '''

    if not os.path.exists(BaseConfigFile):
        print BaseConfigFile ,'is not exist'
        sys.exit(0)

    global GlobalData

    f = open(BaseConfigFile)
    GlobalData = yaml.load(f)
    f.close()

def Find_StepInCases(name,cases):

    for case in cases:
        action = case['action']
        action_name = action['name']
        if action_name == name:
            return case

    return None

def RequestData(addr,arg,method='get',timeout = None):
    '''
        发送http api请求,并保存Cookies
    '''

    global Cookie

    params = {}
    try:
        if Cookie is not None:
            params['cookies'] = Cookie

        if timeout is not None:
            params['timeout'] = timeout

        req = None
        
        if method == 'get':
            params['params'] = arg
            req = requests.get(addr,**params)
        elif method == 'post':
            req = requests.post(addr,data = arg,**params)

        d = req.cookies.get_dict()
        if len(d) > 0:
            Cookie = d
        return req.json()
    except requests.exceptions.Timeout,e:
        raise ValueError(str(e))
    except:
        if REPORT_ERROR_DETAIL:
            if req != None:
                error_detail = {
                    'status_code':req.status_code,
                    'url':req.url,
                    'headers':req.headers,
                    'method':req.request.method,
                    'encoding':req.encoding
                     }
                import pprint
                print "\nerror detail info:"
                pprint.pprint(error_detail)
        raise ValueError('open %s error' % addr)  

def ConfirmData(one,data,add_to_global = True,add_to_name = None,parent = None):
    '''
        验证输出data成员的名称，类型以及是否打印成员值,如果成员类型为list,进一步验证该成员的正确性
    '''
    global GlobalData

    if parent is None:
        parent = 'data'

    dt = one[1]
    is_option = int(dt.get('optional',0))
    if not data.has_key(one[0]):
        if is_option:
            return
        raise ValueError("output member[%s] is not %s's member" %(one[0],parent))

    val = data[one[0]]
    isprint = int(dt.get('print','0'))
    if isprint:
       print '\n',one[0],"'s value:",val

    datatype = type(val).__name__
    
    if datatype == 'unicode':
       datatype = 'str'

    etype = dt['type']
    if datatype != etype:
        raise ValueError('type of member[%s] is %s,but expect member type is %s' % (one[0],datatype,etype))
    
    if dt.has_key('value'):
        if val != dt['value']:
            raise ValueError('value of member[%s] is %s,but expect member value is %s' % (one[0],val,dt['value']))
    
    #是否添加到全部变量中
    if add_to_global:
        if add_to_name is None:
            GlobalData[one[0]] = val
        else:
            GlobalData[add_to_name] = val
   
    #验证list类型成员
    if datatype == "list":
        is_empty = False
        if dt.has_key('length'):
            data_len = int(dt['length'])
            if len(val) != data_len:
                raise ValueError('length of list member[%s] is %d,but expect length is %d'% (one[0],len(val),data_len))
            if 0 == data_len:
                is_empty = True
        if dt.has_key('members') and not is_empty:
            VerifyListMembers(one[0],dt['members'],val)
    
    #验证dict类型成员
    if datatype == "dict":
        if dt.has_key('members'):
            VerifyMapMembers(one[0],dt['members'],val)    

def GetOptData(data):
    del data['code']
    del data['message']
    return data

def ConfirmOutput(data,output):
    '''
        验证输出正确性,包括返回码code, 返回信息message,以及除此之外的其它输出(用data表示)
    '''

    #是否打印输出
    if output.has_key('print'):
        isprint = int(output['print'])
        del output['print']
        if isprint:
            print '\n',data
   
    #配置里面必须包含code和message
    if not data.has_key('code'):
       raise ValueError('return data must have code key')
   
    if not data.has_key('message'):
       raise ValueError('return data must have message key')
    
    ConfirmData(('message',output['message']), data)    

    if not output.has_key('code'):
       raise ValueError('ouput must have code key')

    #验证配置的返回码和真实返回码是否相等
    code = data['code']
    outcode = output['code']
    if code != outcode:
       raise ValueError('return code is %s but expect code is %s' % (code,outcode))

    #获取data值
    optdata = GetOptData(data)
    datalen = len(optdata)
    if output.has_key('data'):
        outdata = output['data']
        if outdata.has_key('print'):
            isprint = int(outdata['print'])
            del outdata['print']
            if isprint:
                print '\n',optdata
        
        #验证返回值(data)的个数是否匹配
   #     outlen = len(output['data'])
    #    if datalen != outlen:
     #       raise ValueError('return data count is %d,but expect data count is %d' % (datalen,outlen))    
        
        #验证配置的每一个data值,包括变量名称,变量类型
        for one in outdata.items():
            add_to_global = False
            add_global_name = None
            if one[1].has_key('export'):
                add_global_name = one[1]['export']
                add_to_global = True
            ConfirmData(one,optdata,add_to_global,add_global_name)
    else:
        if len(optdata) > 0:
            raise ValueError('return data is not empty,but expect data is empty')

 
def RunTest(action):
    '''
        读取每个action,包括地址(由baseConfig里面的serveraddr和当前配置的短地址拼接而成),参数输入,然后发送http api请求，并验证返回的结果是否和配置的一致
    '''
    global GlobalData
    
    mode = action.get('mode','http')
    method = action.get('method','get')
    addr = action['addr']
    params = action.get('input',[])
    data = {}
    for para in params:
        name = para
        val = params[name]
        #输入参数值不是源自赋值，而是来自其它其它变量的值或界面输入值
        if val.has_key('from'):
            frmname = val['from']
            #参数值来源于终端(控制台)输入
            if frmname == 'console':
                data[name] = raw_input('\n%s\'s value ,please input:' % name)
            else:
                #如果不是来自终端输入,则说明来自变量的值,如果在内存里面找不到该变量的值则报错并退出
                if not GlobalData.has_key(frmname):
                    print '\ncanot find ',frmname, '\'s value'
                    sys.exit(0)
                else:
                    #如果在内存里面找到该变量,则赋值给该参数
                    data[name] = GlobalData[frmname]
        else:
            #来自于直接赋值
            data[name] = val['value']

    #baseConfig里面的serveraddr与配置的短地址拼接构成完整的api请求地址
    fulladdr = mode + "://" + GlobalData['serveraddr']
    if GlobalData.has_key("port"):
        fulladdr += ":"
        #端口
        fulladdr += str(GlobalData['port'])

    if addr.startswith('/'):
        fulladdr += addr
    else:
        fulladdr += '/' + addr
    
    timeout = action.get('timeout',None)
    ret = RequestData(fulladdr,data,method,timeout)
    ConfirmOutput(ret,action['output'])

def RunTests():
    '''
        读取test.yaml，并按顺序执行所有action文件
    '''
    
    if not os.path.exists(TestFile):
        print TestFile,'is not exist'
        sys.exit(0)
    
    f = open(TestFile)
    tests = yaml.load(f)
    f.close()

    for test in tests:
        test = test['order']
        action_file = test['file']
        if not os.path.exists(action_file):
            print 'action file:',action_file,'is not exist in',TestFile
            sys.exit(0)
        
        RunAction(action_file)

def RunAction(action_file):
    '''
        执行单个action_file的所有action,按step顺序执行
    '''

    global TotalCases
    global SuccCases
    global FailCases

    if not os.path.exists(action_file):
        print action_file ,'is not exist'
        sys.exit(0)

    f1 = open(action_file)
    steps = yaml.load(f1)
    f1.close()

    casefile = ""
    cases = []
    for step in steps:
        step = step['step']
        #action名称
        step_name = step['name']
        #action重复执行次数
        count = step.get('count',1)
        #action执行完是否退出
        step_exit = step.get('exit',False)
        #action对应的case file名称
        step_file = step.get('file',"")
        if step_file != "":
            casefile = step_file
            f2 = open(casefile)
            cases = yaml.load(f2)
            f2.close()
        
        if casefile == "":
            print 'first step must specify case file in',action_file
            sys.exit(0)
        
        if not os.path.exists(casefile):
            print 'casefle:',casefile ,'is not exist'
            sys.exit(0)

        action = Find_StepInCases(step_name,cases)
        if action is None:
            print step_name,'is not exist,','please make sure step:[',step_name,'] exist in',casefile
            sys.exit(0)

        times = 0
        while True:
            if times == count:
                break
            try:
                print '----------------------------------------------------'
                print 'Run Action[',step_name,']..........',
                #更新缓存实时输出
                sys.stdout.flush()
                TotalCases += 1
                RunTest(action['action'])
                SuccCases += 1
           
                if EnableColor:
                    Color.print_green_text("[PASS]")
                else:
                    print '[PASS]'

            except ValueError,e:
                print "\n" + str(e)
                FailCases += 1
                if EnableColor:
                    Color.print_red_text("FAILED")
                else:
                    print 'FAILED'
                
                #action执行失败,退出
                if EXIT_WHEN_ERROR:
                    sys.exit(0)

            times += 1

        if step_exit:
            sys.exit(0)

if __name__ == "__main__":
    
    if len(sys.argv) > 1:
        for argv in sys.argv:
            if argv == '-e':
                EXIT_WHEN_ERROR = True
            elif argv == "-t":
                REPORT_ERROR_DETAIL = True
    print '-----------------start run tests--------------------'
    start = time.time() 
    LoadBaseConfig()
    RunTests()

    end = time.time()
    runtime = int(end - start)

    print '-------------------------end-------------------------'
    print 'Run Total',TotalCases,'actions in',runtime,'s'
    print 'PASS:',SuccCases,',FAIL:',FailCases
    
