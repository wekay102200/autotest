import sys

if sys.platform == "win32":
    import ctypes

    STD_INPUT_HANDLE = -10
    STD_OUTPUT_HANDLE= -11
    STD_ERROR_HANDLE = -12

    FOREGROUND_BLACK = 0x0
    FOREGROUND_BLUE = 0x01 # text color contains blue.
    FOREGROUND_GREEN = 0x02 # text color contains green.
    FOREGROUND_RED = 0x04 # text color contains red.
    FOREGROUND_INTENSITY = 0x08 # text color is intensified.

    BACKGROUND_BLUE = 0x10 # background color contains blue.
    BACKGROUND_GREEN = 0x20 # background color contains green.
    BACKGROUND_RED = 0x40 # background color contains red.
    BACKGROUND_INTENSITY = 0x80 # background color is intensified.

    std_out_handle = ctypes.windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)

    def set_cmd_color(color, handle=std_out_handle):
        """(color) -> bit
        Example: set_cmd_color(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY)
        """
        bool = ctypes.windll.kernel32.SetConsoleTextAttribute(handle, color)
        return bool

    def reset_color():
        set_cmd_color(FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE)

    def print_red_text(print_text):
        set_cmd_color(FOREGROUND_RED | FOREGROUND_INTENSITY)
        print print_text
        reset_color()

    def print_green_text(print_text):
        set_cmd_color(FOREGROUND_GREEN | FOREGROUND_INTENSITY)
        print print_text
        reset_color()

    def print_blue_text(print_text):
        set_cmd_color(FOREGROUND_BLUE | FOREGROUND_INTENSITY)
        print print_text
        reset_color()

    def print_red_text_with_blue_bg(print_text):
        set_cmd_color(FOREGROUND_RED | FOREGROUND_INTENSITY| BACKGROUND_BLUE | BACKGROUND_INTENSITY)
        print print_text
        reset_color()


elif sys.platform.find('linux') != -1:

    from termcolor import colored

    def print_green_text(text):
        print_color_text("green",text)


    def print_red_text(text):
        print_color_text("red",text)


    def print_blue_text(text):
        print_color_text("blue",text)

    def print_color_text(color,text):

        print colored(text, color)

